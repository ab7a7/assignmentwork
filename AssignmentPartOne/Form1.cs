﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssignmentPartOne
{
    public partial class Form1 : Form
    {
        //Setting Variables
        public float rChan, gChan, bChan;
        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action = false, rectangle, finished;
        private static float xy;
        private Bitmap picture; 
        private Graphics g1, g2;
        private HSB HSBcol = new HSB();

        //Mouse Position Variables
        Point StartPosition = new Point();
        Point EndPosition = new Point();
        
        public Form1()
        {
            InitializeComponent();
            init();
            start();
        }

        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;

            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public float H
            {
                get { return h; }
            }

            public float S
            {
                get { return s; }
            }

            public float B
            {
                get { return b; }
            }

            public int A
            {
                get { return a; }
            }

            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }

            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;

                    float h = hsbColor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }

                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

        }

    

        class HSB
        {//djm added, it makes it simpler to have this code in here than in the C#
            public float rChan, gChan, bChan;
            public HSB()
            {
                rChan = gChan = bChan = 0;
            }
            public void fromHSB(float h, float s, float b)
            {
                float red = b;
                float green = b;
                float blue = b;
                if (s != 0)
                {
                    float max = b;
                    float dif = b * s / 255f;
                    float min = b - dif;

                    float h2 = h * 360f / 255f;

                    if (h2 < 60f)
                    {
                        red = max;
                        green = h2 * dif / 60f + min;
                        blue = min;
                    }
                    else if (h2 < 120f)
                    {
                        red = -(h2 - 120f) * dif / 60f + min;
                        green = max;
                        blue = min;
                    }
                    else if (h2 < 180f)
                    {
                        red = min;
                        green = max;
                        blue = (h2 - 120f) * dif / 60f + min;
                    }
                    else if (h2 < 240f)
                    {
                        red = min;
                        green = -(h2 - 240f) * dif / 60f + min;
                        blue = max;
                    }
                    else if (h2 < 300f)
                    {
                        red = (h2 - 240f) * dif / 60f + min;
                        green = min;
                        blue = max;
                    }
                    else if (h2 <= 360f)
                    {
                        red = max;
                        green = min;
                        blue = -(h2 - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        red = 0;
                        green = 0;
                        blue = 0;
                    }
                }

                rChan = (float)Math.Round(Math.Min(Math.Max(red, 0f), 255));
                gChan = (float)Math.Round(Math.Min(Math.Max(green, 0), 255));
                bChan = (float)Math.Round(Math.Min(Math.Max(blue, 0), 255));

            }
        }

        
        public void init() // all instances will be prepared
        {
            //HSBcol = new HSB();
            finished = false;
            //set Parameter for the picture
            x1 = 640;
            y1 = 480;
            xy = (float)x1 / (float)y1;
            picture = new Bitmap(x1, y1);
            g1 = Graphics.FromImage(picture);
            finished = true;
        }

        public void start()
        {
            action = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;
            Pen myPen2 = new Pen(Color.Black);

            action = false;


            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightness
                        ///djm added
                        ///HSBcol.fromHSB(h,0.8f,b); //convert hsb to rgb then make a Java Color
                        ///Color col = new Color(0,HSBcol.rChan,HSBcol.gChan,HSBcol.bChan);
                        ///g1.setColor(col);
                        //djm end
                        //djm added to convert to RGB from HSB

                        // g1.setColor(Color.getHSBColor(h, 0.8f, b));
                        //djm test


                        //  Color col = Color.getHSBColor(h, 0.8f, b);

                        HSBColor hsb = new HSBColor(h * 255, 0.8f * 255, b * 255);
                        myPen2 = new Pen(hsb.Color); //create a pen object

                        // int red = col.getRed();
                        // int green = col.getGreen();
                        //int blue = col.getBlue();

                        //djm 
                        alt = h;
                    }
                    g1.DrawLine(myPen2, x, y, x + 1, y);
                }
            ///showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
            ///setCursor(c2);
            action = true;
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            Cursor.Current = Cursors.Cross;


            // Make the mouse set to true to tell us mouse is pressed.
            rectangle = true;
            // Store the starting mouse position
            StartPosition.X = e.X;
            StartPosition.Y = e.Y;
            // Special value lets us know that no previous
            // rectangle needs to be erased.
            EndPosition.X = -1;
            EndPosition.Y = -1;
        }

        private void MyDrawReversibleRectangle(Point p1, Point p2)
        {
            Rectangle rc = new Rectangle();

            // get screen points
            p1 = PointToScreen(p1);
            p2 = PointToScreen(p2);
            // check if the rectangle
            if (p1.X < p2.X)
            {
                rc.X = p1.X;
                rc.Width = p2.X - p1.X;
            }
            else
            {
                rc.X = p2.X;
                rc.Width = p1.X - p2.X;
            }
            if (p1.Y < p2.Y)
            {
                rc.Y = p1.Y;
                rc.Height = p2.Y - p1.Y;
            }
            else
            {
                rc.Y = p2.Y;
                rc.Height = p1.Y - p2.Y;
            }
            // Draw the reversible frame.
            ControlPaint.DrawReversibleFrame(rc,
                            Color.Red, FrameStyle.Dashed);
        }


        public void paint(Graphics g)
        {
            update(g);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            g2 = e.Graphics; //graphics context for the form (which you see)
            g2.DrawImageUnscaled(picture, 0, 0); //puts bitmap onto the form so you can see it
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {

                int z, w;

                if (action)
                {
                    xe = e.X;
                    ye = e.Y;
                    if (xs > xe)
                    {
                        z = xs;
                        xs = xe;
                        xe = z;
                    }
                    if (ys > ye)
                    {
                        z = ys;
                        ys = ye;
                        ye = z;
                    }
                    w = (xe - xs);
                    z = (ye - ys);
                    if ((w < 2) && (z < 2)) {
                        initvalues();
                    }
                    else
                    {
                        if (((float)w > (float)z * xy))
                        {
                            ye = (int)((float)ys + (float)w / xy);
                        }
                        else
                        {
                            xe = (int)((float)xs + (float)z * xy);
                        }
                        xende = xstart + xzoom * (double)xe;
                        yende = ystart + yzoom * (double)ye;
                        xstart += xzoom * (double)xs;
                        ystart += yzoom * (double)ys;
                    }
                    xzoom = (xende - xstart) / (double)x1;
                    yzoom = (yende - ystart) / (double)y1;

                    // to tell us mouse is not pressed
                    rectangle = false;
                    // draw again over the lines to ensure the rectangle is gone
                    if (EndPosition.X != -1)
                    {
                        Point ptCurrent = new Point(e.X, e.Y);
                        MyDrawReversibleRectangle(StartPosition, EndPosition);
                    }
                    // no line to delete or draw over.
                    EndPosition.X = -1;
                    EndPosition.Y = -1;
                    StartPosition.X = -1;
                    StartPosition.Y = -1;

                    mandelbrot();
                    this.Refresh();
            }
        
        }

        public void update(Graphics g)
        {
            g.DrawImage(picture, 0, 0);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {

            Point CurrentPosition = new Point(e.X, e.Y);
            // If we "have the mouse", then we draw our lines.
            if (rectangle)
            {
                // If we have drawn previously, draw again in
                // that spot to remove the lines.
                if (EndPosition.X != -1)
                {
                    MyDrawReversibleRectangle(StartPosition, EndPosition);
                }
                // Update last point.
                EndPosition = CurrentPosition;
                // Draw new lines.
                MyDrawReversibleRectangle(StartPosition, CurrentPosition);
            }
        }

        

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveFileBox = new SaveFileDialog()) 
            {
                saveFileBox.InitialDirectory = "C:\\Users\\c3369363\\Desktop";
                saveFileBox.Filter = "BMP Files|*.bmp";
                if (saveFileBox.ShowDialog(this) == DialogResult.OK)
                {
                    picture.Save(saveFileBox.FileName, ImageFormat.Bmp);
                }
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog open = new OpenFileDialog();
                open.Filter = "BMP Files|*.bmp";
                if (open.ShowDialog() == DialogResult.OK)
                {
                    picture = new Bitmap(open.FileName);
                    g1 = Graphics.FromImage(picture);
                    start();
                    
                }
            }
            catch (Exception)
            {
                throw new ApplicationException("Failed loading image");
            }
        }     
    }
}
